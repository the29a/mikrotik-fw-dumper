Создает локальный репозиторий прошивок и дополнительных пакетов для Mikrotik
<br>
По умолчанию скачивает все npk и zip, доступные на https://mikrotik.com/download
<br>


<h>Использование:</h>
<p>git clone https://github.com/the29a/mikrotik-fw-dumper.git ; cd mikrotik-fw-dumper</p>
<p><code>chmod +x mt-local-repo.sh</code></p>
<p><code>./mt-local-repo.sh</code></p>

<h>Замеченые баги:</h>
<p>Повторно скачивает файлы для x86</p>
<p>На 10.03.2018 закрыт доступ к версии прошивки 5.26.</p>
