#/bin/bash
#Кря
#Вы всё равно не читаете исходники скриптов
mkdir download
cd download

echo "Downloading Mikrotik firwmare and packages."
#Получаем список прошивок
touch fw.list ; curl -s https://mikrotik.com/download | grep -Eo "//[a-zA-Z0-9./?=_-]*" | grep npk | cut -c 3- | sort > fw.list ;

#Добавляем http
sed -i '/^http:\/\//!s/^/http:\/\//' fw.list

#Получаем список пакетов
touch pkg.list ; curl -s https://mikrotik.com/download | grep -Eo "//[a-zA-Z0-9./?=_-]*" | grep zip | cut -c 3- | sort > pkg.list ;

#Добавляем http
sed -i '/^http:\/\//!s/^/http:\/\//' pkg.list

#Скачиваем прошивки и пакеты
echo "Download firmware"
wget -N -nv -c -i fw.list -P fw
echo "Download packages"
wget -N -nv -c -i pkg.list -P pkg
#Надо сделать progressbar, но у меня лапки

echo "Done."
#¯\_(ツ)_/¯
